using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Moq;
using OficiosApi.Application.Services.Review;
using OficiosApi.Controllers;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models.Input.Review;
using Xunit;
using Xunit.Abstractions;

namespace OficiosApi.Tests
{
    public class ReviewTests
    {
        private const int NonAnonymousReviewId = 1;
        private const int AnonymousReviewId = 2;
        
        private DbContextOptions<ApplicationDbContext> ContextOptions { get; }

        public ReviewTests(ITestOutputHelper testOutputHelper)
        {
            ContextOptions = new DbContextOptionsBuilder<ApplicationDbContext>()
                .UseInMemoryDatabase("OficiosTestDatabase")
                .Options;

            Seed();
        }

        private void Seed()
        {
            using var context = new ApplicationDbContext(ContextOptions);
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();

            var tradesmanQuality1 = new TradesmanQuality
            {
                TradesmanQualityId = "1",
                Label = "TestQuality1"
            };

            var tradesmanQuality2 = new TradesmanQuality
            {
                TradesmanQualityId = "2",
                Label = "TestQuality2"
            };

            var tradesmanQuality3 = new TradesmanQuality
            {
                TradesmanQualityId = "3",
                Label = "TestQuality3"
            };

            context.Add(tradesmanQuality1);
            context.Add(tradesmanQuality2);
            context.Add(tradesmanQuality3);

            var client = new Client
            {
                Id = 1,
                FirstName = "TestClient1",
                LastName = "TestClient2"
            };

            context.Add(client);

            var tradesman = new Tradesman
            {
                Id = 1
            };

            context.Add(tradesman);

            var qualityScore1 = new TradesmanQualityScore
            {
                Score = 4,
                TradesmanQuality = tradesmanQuality1,
                TradesmanQualityScoreId = "2"
            };

            var qualityScore2 = new TradesmanQualityScore
            {
                Score = 3,
                TradesmanQuality = tradesmanQuality2,
                TradesmanQualityScoreId = "3"
            };


            var review = new Review
            {
                Id = NonAnonymousReviewId,
                Tradesman = tradesman,
                Client = client,
                QualityScores = new List<TradesmanQualityScore>()
                {
                    qualityScore1,
                    qualityScore2
                },
                Anonymous = false
            };
            
            context.Add(review);

            var anonymousReview = new Review
            {
                Id = AnonymousReviewId,
                Tradesman = tradesman,
                Client = client,
                QualityScores = new List<TradesmanQualityScore>()
                {
                    qualityScore1,
                    qualityScore2
                },
                Anonymous = true
            };

            context.Add(anonymousReview);

            context.SaveChanges();
        }

        [Fact]
        public void Can_create_review()
        {
            // Arrange
            using var dbContext = new ApplicationDbContext(ContextOptions);
            var reviewCreator = new ReviewCreator(dbContext);

            // Act
            var review = reviewCreator.CreateReview(new ReviewCreator.ReviewCreatorParameters
            {
                TradesmanId = 1,
                ClientId = 1,
                QualityScores = new List<TradesmanQualityScoreViewModel>
                {
                    new TradesmanQualityScoreViewModel
                    {
                        TradesmanQualityId = "1",
                        Score = 2.25f
                    }
                },
                Text = "Lorem ipsum"
            });

            // Assert
            Assert.Contains(review, dbContext.Reviews);
        }

        [Fact]
        public void Can_update_review()
        {
            // Arrange
            using var dbContext = new ApplicationDbContext(ContextOptions);
            var reviewUpdater = new ReviewUpdater(dbContext);
            var review = dbContext.Reviews.Find(1);

            // Act
            reviewUpdater.UpdateReview(1, new ReviewUpdateViewModel
            {
                Text = "New text",
                QualityScores = new List<TradesmanQualityScoreViewModel>
                {
                    new TradesmanQualityScoreViewModel
                    {
                        Score = 2.5f,
                        TradesmanQualityId = "1"
                    },
                    new TradesmanQualityScoreViewModel
                    {
                        TradesmanQualityId = "3",
                        Score = 4f
                    }
                }
            });

            // Assert
            var tradesmanQualityScoreStored1 = review.QualityScores.Find(qs =>
                qs.TradesmanQuality.TradesmanQualityId == "1");

            var tradesmanQualityScoreStored2 = review.QualityScores.Find(qs =>
                qs.TradesmanQuality.TradesmanQualityId == "2");

            var tradesmanQualityScoreStored3 = review.QualityScores.Find(qs =>
                qs.TradesmanQuality.TradesmanQualityId == "3");

            Assert.NotNull(tradesmanQualityScoreStored1);
            Assert.Equal(2.5f, tradesmanQualityScoreStored1.Score);

            Assert.Null(tradesmanQualityScoreStored2);
            Assert.NotNull(tradesmanQualityScoreStored3);
        }

        [Fact]
        public void Can_delete_review()
        {
            // Arrange
            using var dbContext = new ApplicationDbContext(ContextOptions);
            var reviewDeleter = new ReviewDeleter(dbContext);
            const int reviewIdToDelete = 1;

            // Act
            reviewDeleter.DeleteReview(reviewIdToDelete);
            
            // Assert
            var review = dbContext.Reviews.FirstOrDefault(r => r.Id == reviewIdToDelete);
            Assert.Null(review);
        }

        [Fact]
        public void User_role_can_get_tradesman_reviews()
        {
            // Arrange
            using var dbContext = new ApplicationDbContext(ContextOptions);

            var webHostingEnvironmentMock = new Mock<IWebHostEnvironment>().Object;
            var userStoreMock = new Mock<IUserStore<ApplicationUser>>().Object;
            var userManagerMock = new Mock<UserManager<ApplicationUser>>(userStoreMock,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null).Object;

            const int tradesmanId = 1;

            // Act
            var tradesmenController = new TradesmenController(dbContext, webHostingEnvironmentMock, userManagerMock);


            // Assert
            var actionResult = tradesmenController.GetReviewsForUser(tradesmanId);

            Assert.NotEmpty(actionResult);
        }

        [Fact]
        public void Tradesmen_reviews_method_for_clients_does_not_return_client_information_when_review_is_anonymous()
        {
            // Arrange
            using var dbContext = new ApplicationDbContext(ContextOptions);

            var webHostingEnvironmentMock = new Mock<IWebHostEnvironment>().Object;
            var userStoreMock = new Mock<IUserStore<ApplicationUser>>().Object;

            var userManagerMock = new Mock<UserManager<ApplicationUser>>(userStoreMock,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null).Object;
            
            const int tradesmanId = 1;

            // Act
            var tradesmenController = new TradesmenController(dbContext, webHostingEnvironmentMock, userManagerMock);


            // Assert
            var actionResult = tradesmenController.GetReviewsForUser(tradesmanId);

            var anonymousReview = actionResult.First(r => Convert.ToInt32(r.ReviewId) == AnonymousReviewId);
            
            Assert.Null(anonymousReview.Client);
        }
        
        [Fact]
        public void Tradesmen_reviews_method_for_clients_return_client_information_when_review_is_not_anonymous()
        {
            // Arrange
            using var dbContext = new ApplicationDbContext(ContextOptions);

            var webHostingEnvironmentMock = new Mock<IWebHostEnvironment>().Object;
            var userStoreMock = new Mock<IUserStore<ApplicationUser>>().Object;

            var userManagerMock = new Mock<UserManager<ApplicationUser>>(userStoreMock,
                null,
                null,
                null,
                null,
                null,
                null,
                null,
                null).Object;
            const int tradesmanId = 1;

            // Act
            var tradesmenController = new TradesmenController(dbContext, webHostingEnvironmentMock, userManagerMock);


            // Assert
            var actionResult = tradesmenController.GetReviewsForUser(tradesmanId);

            var review = actionResult.First(r => Convert.ToInt32(r.ReviewId) == NonAnonymousReviewId);
            
            Assert.NotNull(review.Client);
            Assert.NotNull(review.Client.FirstName);
            Assert.NotNull(review.Client.LastName);
        }
    }
}
