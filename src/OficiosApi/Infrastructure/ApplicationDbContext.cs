using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Domain;

namespace OficiosApi.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Client> Clients{ get; set; }
        public DbSet<Tradesman> Tradesmen { get; set; }
        public DbSet<State> States { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<SkilledTrade> SkilledTrades { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<Token> Tokens { get; set; }
        public DbSet<TradesmanQuality> TradesmanQualities { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            // Using serial columns because the Elephant Server SQL server version doesn't support the newer IDENTITY strategy
            modelBuilder.UseSerialColumns();

            modelBuilder.Entity<TradesmanSkilledTrade>()
                .HasKey(ts => new { ts.TradesmanId, ts.SkilledTradeId });

            modelBuilder.Entity<TradesmanSkilledTrade>()
                .HasOne(t => t.SkilledTrade)
                .WithMany()
                .HasForeignKey(t => t.SkilledTradeId)
                .OnDelete(DeleteBehavior.Restrict);
            
            modelBuilder
                .Entity<TradesmanQuality>()
                .Property(e => e.TradesmanQualityId)
                .ValueGeneratedOnAdd();

            modelBuilder
                .Entity<TradesmanQualityScore>()
                .Property(e => e.TradesmanQualityScoreId)
                .ValueGeneratedOnAdd();
        }
    }
}
