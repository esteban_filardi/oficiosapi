using System.Linq;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Infrastructure;

namespace OficiosApi.Application.Services.Review
{
    public class ReviewDeleter
    {
        private readonly ApplicationDbContext _dbContext;

        public ReviewDeleter(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void DeleteReview(int reviewId)
        {
            var review = _dbContext.Reviews
                .Include(r => r.Tradesman)
                .SingleOrDefault(r => r.Id == reviewId);


            if (review == null) return;
            
            review.Tradesman.Reviews.Remove(review);
            review.Tradesman.UpdateAverageScore();
            _dbContext.Reviews.Remove(review);
                
            _dbContext.SaveChanges();
        }
    }
}
