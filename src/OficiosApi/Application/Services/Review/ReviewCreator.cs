using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models.Input.Review;

namespace OficiosApi.Application.Services.Review
{
    public class ReviewCreator
    {
        private readonly ApplicationDbContext _dbContext;
        
        public ReviewCreator(ApplicationDbContext context)
        {
            _dbContext = context;
        }

        public Domain.Review CreateReview(ReviewCreatorParameters parameters)
        {
            var tradesman = _dbContext.Tradesmen
                .Include(t => t.Reviews)
                .SingleOrDefault(t => t.Id == parameters.TradesmanId);

            if (null == tradesman)
            {
                throw new Exception("Invalid tradesman ID");
            }

            var client = _dbContext.Clients.Find(parameters.ClientId);

            if (client == null)
            {
                throw new Exception("Invalid client ID");
            }
            
            var now = DateTime.Now;
            var review = new Domain.Review {Text = parameters.Text,
                CreatedDate = now,
                LastModifiedDate = now,
                Client = client,
                Anonymous = parameters.Anonymous
            };

            foreach (var qualityScoreInRequest in parameters.QualityScores)
            {
                var tradesmanQuality = new TradesmanQuality
                {
                    TradesmanQualityId = qualityScoreInRequest.TradesmanQualityId
                };
                _dbContext.Attach(tradesmanQuality);
                
                var qualityScore = new TradesmanQualityScore
                {
                    TradesmanQuality = tradesmanQuality,
                    Score = qualityScoreInRequest.Score
                };
                
                review.QualityScores.Add(qualityScore);
            }

            review.UpdateAverageScore();
            
            tradesman.Reviews.Add(review);
            tradesman.UpdateAverageScore();

            _dbContext.Tradesmen.Update(tradesman);
            _dbContext.SaveChanges();

            return review;
        }
        
        public class ReviewCreatorParameters
        {
            public int TradesmanId { get; set; }
            public int ClientId { get; set; }
            public IList<TradesmanQualityScoreViewModel> QualityScores { get; set; }
            public string Text { get; set; }
            public bool Anonymous { get; set; }
        }
    }
}
