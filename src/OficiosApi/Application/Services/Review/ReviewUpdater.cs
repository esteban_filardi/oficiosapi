using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models.Input.Review;

namespace OficiosApi.Application.Services.Review
{
    public class ReviewUpdater
    {
        private readonly ApplicationDbContext _dbContext;

        public ReviewUpdater(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public void UpdateReview(int reviewId, ReviewUpdateViewModel reviewUpdateData)
        {
            var review = _dbContext.Reviews
                .Include(t => t.Tradesman)
                .ThenInclude(t => t.Reviews)
                .Include(r => r.QualityScores)
                .ThenInclude(qs => qs.TradesmanQuality)
                .SingleOrDefault(r => r.Id == reviewId);

            if (review == null)
            {
                throw new ReviewNotFoundException();
            }

            review.Text = reviewUpdateData.Text;
            review.Anonymous = reviewUpdateData.Anonymous;
            UpdateScores(reviewUpdateData, review);

            _dbContext.SaveChanges();
        }

        private static void UpdateScores(ReviewUpdateViewModel reviewUpdateData, Domain.Review review)
        {
            review.QualityScores.RemoveAll(qs =>
                !reviewUpdateData.QualityScores.Exists(uqs =>
                    uqs.TradesmanQualityId == qs.TradesmanQuality.TradesmanQualityId)
            );

            foreach (var receivedQualityScores in reviewUpdateData.QualityScores)
            {
                var currentQualityScore = review.QualityScores.SingleOrDefault(ts =>
                    ts.TradesmanQuality.TradesmanQualityId == receivedQualityScores.TradesmanQualityId);

                // The quality already exists, just update the score and continue
                if (currentQualityScore != null)
                {
                    currentQualityScore.Score = receivedQualityScores.Score;
                    continue;
                }

                // The quality didn't exist, add the new quality score
                review.QualityScores.Add(new TradesmanQualityScore
                {
                    Score = receivedQualityScores.Score,
                    TradesmanQuality = new TradesmanQuality
                    {
                        TradesmanQualityId = receivedQualityScores.TradesmanQualityId
                    }
                });
            }

            review.UpdateAverageScore();
            review.Tradesman.UpdateAverageScore();
        }

        public class ReviewNotFoundException : Exception
        {
        }
    }
}