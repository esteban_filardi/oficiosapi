using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Infrastructure;
using OficiosApi.Models;
using OficiosApi.Models.Input.Tradesman;

namespace OficiosApi.Application.Services.Tradesman
{
    /// <summary>
    /// Method that retrieves the tradesmen for the search functionality.
    /// The results are paginated and sorted according to a similar rule used by IMDB.
    /// </summary>
    public class TradesmenQueryService
    {
        // Parameters of the IMDB rating algorithm
        private const double AverageScoreOfAllReviews = 2.5;
        private const int MinimumVotesRequiredToBeConsidered = 10;
        
        private readonly ApplicationDbContext _context;

        public TradesmenQueryService(ApplicationDbContext dbContext)
        {
            _context = dbContext;
        }

        public QueryResult QueryTradesmen(TradesmenRequestDto tradesmenRequestDto, int resultsPerPage, int page)
        {
            var efQuery = _context.Tradesmen
                .Include(t => t.TradesmanSkilledTrades)
                .ThenInclude(ts => ts.SkilledTrade)
                .Include(t => t.City)
                .ThenInclude(c => c.State)
                .Include(t => t.User)
                .Include(t => t.Reviews)
                .AsQueryable();

            efQuery = AddFilterConditions(tradesmenRequestDto, efQuery);

            var queryResult = new QueryResult {TotalCount = efQuery.Count()};

            var efQueryResults = efQuery.ToArray();

            SortResult(efQueryResults);
            
            queryResult.PageResults = efQueryResults.Skip((page - 1) * resultsPerPage)
                .Take(resultsPerPage);

            return queryResult;
        }

        private static IQueryable<Domain.Tradesman> AddFilterConditions(TradesmenRequestDto tradesmenRequestDto, IQueryable<Domain.Tradesman> efQuery)
        {
            if (tradesmenRequestDto.skilledTradeId != null)
            {
                efQuery = efQuery.Where(t =>
                    t.TradesmanSkilledTrades.Any(ts => ts.SkilledTradeId == tradesmenRequestDto.skilledTradeId));
            }

            if (tradesmenRequestDto.cityId != null)
            {
                efQuery = efQuery.Where(t => t.City.Id == tradesmenRequestDto.cityId);
            }

            if (!string.IsNullOrWhiteSpace(tradesmenRequestDto.FirstName))
            {
                efQuery = efQuery.Where(t => t.Name.Contains(tradesmenRequestDto.FirstName));
            }
            
            if (!string.IsNullOrWhiteSpace(tradesmenRequestDto.LastName))
            {
                efQuery = efQuery.Where(t => t.LastName.Contains(tradesmenRequestDto.LastName));
            }
            
            if (!string.IsNullOrWhiteSpace(tradesmenRequestDto.Email))
            {
                efQuery = efQuery.Where(t => t.User.Email.Contains(tradesmenRequestDto.Email));
            }

            if (!string.IsNullOrWhiteSpace(tradesmenRequestDto.Phone))
            {
                efQuery = efQuery.Where(t => t.PhoneNumber
                    .Replace("(", "")
                    .Replace(")", "")
                    .Replace("-", "")
                    .Replace( " ", "" )
                    .Replace( "r", "" )
                    .Replace( "n", "" )
                    .Replace( "t", "" )
                    .Contains(tradesmenRequestDto.Phone));
            }

            return efQuery;
        }

        private static void SortResult(Domain.Tradesman[] resultArray)
        {
            Array.Sort(resultArray, (Domain.Tradesman tradesmanLeft, Domain.Tradesman tradesmanRight) =>
            {
                var scoreLeft = GetSortingScoreForTradesman(tradesmanLeft);
                var scoreRight = GetSortingScoreForTradesman(tradesmanRight);

                if (scoreLeft < scoreRight)
                {
                    return 1;
                }
                else if (scoreLeft > scoreRight)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            });
        }

        private static double GetSortingScoreForTradesman(Domain.Tradesman tradesman)
        {
            var commonDividend = (double) tradesman.Reviews.Count + MinimumVotesRequiredToBeConsidered;

            if (tradesman.AverageScore == null)
            {
                return 0;
            }
            else
            {
                var score = (tradesman.Reviews.Count / commonDividend) * (double) tradesman.AverageScore +
                               (MinimumVotesRequiredToBeConsidered / commonDividend) * AverageScoreOfAllReviews;

                return score;
            }
        }
        
        public IEnumerable<TradesmanForAdmin> QueryTradesmenForAdmin()
        {
            var tradesmen = _context.Tradesmen
                .Include(t => t.User)
                .Include(t => t.City)
                    .ThenInclude(c => c.State)
                .ToArray();

            var tradesmenForAdmin = new List<TradesmanForAdmin>(tradesmen.Length);
            tradesmenForAdmin.AddRange(tradesmen.Select(
                tradesman => new TradesmanForAdmin {
                    Id = tradesman.Id, 
                    FirstName = tradesman.Name,
                    LastName = tradesman.LastName,
                    CityAndState = tradesman.City?.Name + " - " + tradesman.City?.State.Name,
                    CreatedDate = tradesman.CreatedAt,
                    Blocked = tradesman.User.Blocked
                })
            );

            return tradesmenForAdmin.ToArray();
        }

        public class QueryResult
        {
            public int TotalCount { get; set; }
            public IEnumerable<Domain.Tradesman> PageResults { get; set; }
        }
    }
}