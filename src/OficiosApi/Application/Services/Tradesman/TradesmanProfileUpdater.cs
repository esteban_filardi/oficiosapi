using System;
using System.IO;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models.Input.Tradesman.TradesmanUpdateProfile;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace OficiosApi.Application.Services.Tradesman
{
    public class TradesmanProfileUpdater
    {
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly HttpRequest _request;

        public TradesmanProfileUpdater(ApplicationDbContext context, IWebHostEnvironment hostingEnvironment,
            HttpRequest request)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _request = request;
        }

        public void UpdateProfile(int id, TradesmanUpdateProfileDto tradesmanUpdateProfileDto)
        {
            var tradesman = _context.Tradesmen
                .Include(t => t.TradesmanSkilledTrades)
                .Include(t => t.City)
                .Include(t => t.User)
                .SingleOrDefault(t => t.Id == id);

            if (tradesman == null)
            {
                throw new TradesmanNotFoundException();
            }                     
            
            tradesman.Name = tradesmanUpdateProfileDto.FirstName;
            tradesman.LastName = tradesmanUpdateProfileDto.LastName;
            tradesman.LastName = tradesmanUpdateProfileDto.LastName;
            tradesman.User.Email = tradesman.User.UserName = tradesmanUpdateProfileDto.Email;
            tradesman.PhoneNumber = tradesmanUpdateProfileDto.PhoneNumber;
            tradesman.ProfileMarkup = ReplaceServerUrlForPlaceHolder(tradesmanUpdateProfileDto.ProfileMarkup);

            tradesman.TradesmanSkilledTrades.RemoveAll(ts =>
                !tradesmanUpdateProfileDto.SkilledTradeIds.Contains(ts.SkilledTradeId));
            
            foreach (var skilledTradeId in tradesmanUpdateProfileDto.SkilledTradeIds)
            {
                if (tradesman.TradesmanSkilledTrades.Exists(ts => ts.SkilledTradeId == skilledTradeId)) {
                    continue;
                }
                
                var skilledTrade = _context.SkilledTrades
                    .SingleOrDefault(s => s.Id == skilledTradeId);

                var tradesmanSkilledTrade = new TradesmanSkilledTrade {SkilledTrade = skilledTrade};
                
                tradesman.TradesmanSkilledTrades.Add(tradesmanSkilledTrade);
            }

            if (tradesmanUpdateProfileDto.ProfileImage != null)
            {
                var webRootPath = _hostingEnvironment.WebRootPath;
                var newPath = Path.Combine(webRootPath, "api", "uploads");

                if (tradesmanUpdateProfileDto.ProfileImage != null)
                {
                    var filename = tradesmanUpdateProfileDto.ProfileImage.Filename;

                    var imageBytes =
                        Convert.FromBase64String(tradesmanUpdateProfileDto.ProfileImage.Base64EncodedImage);
                    var extension = "." + filename.Split('.')[filename.Split('.').Length - 1];
                    var fileName = Guid.NewGuid() + extension; // Create a new filename 

                    var fullPath = Path.Combine(newPath, fileName);

                    using (var imageFile = new FileStream(fullPath, FileMode.Create))
                    {
                        imageFile.Write(imageBytes, 0, imageBytes.Length);
                        imageFile.Flush();
                    }

                    tradesman.ImageProfilePath = fileName;
                }
            }

            _context.SaveChanges();
        }

        private string ReplaceServerUrlForPlaceHolder(string profileMarkup)
        {
            return profileMarkup?.Replace(GetServerUrl(), "{{ serverUrl }}");
        }

        private string GetServerUrl()
        {
            return $"{_request.Scheme}://{_request.Host}{_request.PathBase}";
        }

        public class TradesmanNotFoundException : Exception
        {
        }
    }
}