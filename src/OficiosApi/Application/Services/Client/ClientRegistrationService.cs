using System;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MimeKit;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models;
using OficiosApi.Models.Input;

namespace OficiosApi.Application.Services.Client
{
    public class ClientRegistrationService
    {
        public const string RoleName = "Client";
        
        private readonly ApplicationDbContext _dbContext;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public ClientRegistrationService(
            ApplicationDbContext context,
            IRazorViewToStringRenderer razorViewToStringRenderer,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration)
        {
            _dbContext = context;
            _razorViewToStringRenderer = razorViewToStringRenderer;
            _userManager = userManager;
            _configuration = configuration;
        }
        
        public async Task RegisterClient(ClientRegistrationRequestDto clientRegistrationRequest)
        {
            var emailWithRolePrefix = $"{RoleName}-{clientRegistrationRequest.Email}";
            
            // check if the Username/Email already exists
            var user = await _userManager.FindByNameAsync(emailWithRolePrefix);
            if (user != null)
            {
                throw new EmailAlreadyBeingUsedException();
            }

            // if (!PasswordCheck.IsValidPassword(model.Password, _userManager.Options.Password)) return BadRequest("Password is too weak.");

            var now = DateTime.Now;

            user = await CreateUser(clientRegistrationRequest, emailWithRolePrefix, now);

            var client = CreateClient(clientRegistrationRequest, user, now);

            await _dbContext.Clients.AddAsync(client);
            
            var confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(client.User);
            
            await _dbContext.SaveChangesAsync();

            try
            {
                await SendRegistrationEmail(client, confirmationToken);
            }
            catch (Exception)
            {
                // if the email sending fails, return a successful response anyway
            }
        }

        private Domain.Client CreateClient(ClientRegistrationRequestDto clientRegistrationRequest, ApplicationUser user, DateTime now)
        {
            var client = new Domain.Client
            {
                FirstName = clientRegistrationRequest.FirstName,
                LastName = clientRegistrationRequest.LastName,
                User = user,
                CreatedDate = now,
                LastModifiedDate = now
            };
            return client;
        }

        private async Task<ApplicationUser> CreateUser(
            ClientRegistrationRequestDto clientRegistrationRequest,
            string emailWithClassPrefix,
            DateTime now)
        {
            // create a new Item with the client-sent json data
            var user = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = emailWithClassPrefix,
                Email = clientRegistrationRequest.Email,
                DisplayName = clientRegistrationRequest.FirstName,
                CreatedDate = now,
                LastModifiedDate = now
            };

            // Add the user to the Db with the chosen password
            await _userManager.CreateAsync(user, clientRegistrationRequest.Password);

            // Assign the user to the 'RegisteredUser' role.
            await _userManager.AddToRoleAsync(user, RoleName);

            // Remove Lockout and E-Mail confirmation
            user.EmailConfirmed = false;
            user.LockoutEnabled = true;
            
            return user;
        }
        
        private async Task SendRegistrationEmail(Domain.Client client, string confirmationToken)
        {
            var message = new MimeMessage ();
            message.From.Add (new MailboxAddress ("TodoOficios", "esteban.filardi@gmail.com"));
            message.To.Add (new MailboxAddress ($"{client.FirstName}  {client.LastName}", 
                client.User.Email.Replace($"{RoleName}-", "")));
            message.Subject = "TodoOficios - Confirme su correo electrónico";

            var confirmAccountEmail = new ConfirmAccountViewModel
            {
                UserId = client.User.Id,
                UserType = "tradesman",
                FirstName = client.FirstName,
                LastName = client.LastName,
                ConfirmationToken = confirmationToken,
                ClientUrl = _configuration.GetValue<string>("ClientUrl")
            };

            var bodyBuilder = new BodyBuilder
            {
                HtmlBody = await _razorViewToStringRenderer.RenderViewToStringAsync(
                    "/Views/ConfirmAccountEmail.cshtml", confirmAccountEmail)
            };

            message.Body = bodyBuilder.ToMessageBody();

            using (var smtpClient = new SmtpClient ()) 
            {
                smtpClient.Connect ("in-v3.mailjet.com", 587, false);
                var username = _configuration.GetValue<string>("Email:Smtp:Username");
                var password = _configuration.GetValue<string>("Email:Smtp:Password");
                smtpClient.Authenticate(username, password);
                smtpClient.Send(message);
                smtpClient.Disconnect(true);
            }
        }
    }
    

    public class EmailAlreadyBeingUsedException : Exception
    {
    }
}
