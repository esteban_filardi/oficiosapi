using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Newtonsoft.Json;

namespace OficiosApi.Domain {    
    public class City   
    {
        public int Id { get; set; }
        public string Code { get; set; }
        [Required]
        [JsonIgnoreAttribute]
        public State State { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
