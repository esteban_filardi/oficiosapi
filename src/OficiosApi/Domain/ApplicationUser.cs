using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OficiosApi.Domain
{
    public class ApplicationUser : IdentityUser
    {
        public string DisplayName { get; set; }
        
        [Required]
        public DateTime CreatedDate { get; set; }

        [Required]
        public DateTime LastModifiedDate { get; set; }

        public bool Blocked { get; set; }
        
        /// <summary>
        /// A list of all the refresh tokens issued for this users.
        /// </summary>
        public List<Token> Tokens { get; set; }
    }
}
