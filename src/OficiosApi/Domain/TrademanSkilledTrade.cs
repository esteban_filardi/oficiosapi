using Newtonsoft.Json;

namespace OficiosApi.Domain 
{
    public class TradesmanSkilledTrade 
    {
        [JsonIgnoreAttribute]
        public int TradesmanId { get; set; }
        [JsonIgnoreAttribute]
        public Tradesman Tradesman { get; set; }

        [JsonIgnoreAttribute]
        public int SkilledTradeId { get; set; }        
        public SkilledTrade SkilledTrade { get; set; }
    }
}
