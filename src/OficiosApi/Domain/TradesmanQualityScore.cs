namespace OficiosApi.Domain
{
    public class TradesmanQualityScore
    {
        public string TradesmanQualityScoreId { get; set; }
        public TradesmanQuality TradesmanQuality { get; set; }
        public float Score { get; set; }
    }
}
