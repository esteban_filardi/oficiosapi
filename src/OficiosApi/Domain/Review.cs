using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;

namespace OficiosApi.Domain {    
    public class Review   
    {
        public int Id { get; set; }
        [JsonIgnoreAttribute]
        public Tradesman Tradesman { get; set; }
        public float? AverageScore { get; set; }
        public List<TradesmanQualityScore> QualityScores{ get; set; }
        public string Text { get; set; }
        public bool Anonymous { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public Client Client { get; set; }

        public Review()
        {
            QualityScores = new List<TradesmanQualityScore>();
        }
        
        public void UpdateAverageScore()
        {
            if (QualityScores.Count == 0)
            {
                AverageScore = null;
            }
            
            var accumulator = QualityScores.Sum(qualityScore => qualityScore.Score);
            
            AverageScore =  accumulator / QualityScores.Count;
        }
    }
}
