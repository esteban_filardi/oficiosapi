using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OficiosApi.Domain
{
    public class SkilledTrade
    {
        public int Id { get; set; }
        [Required]        
        public string Name { get; set; }
    }
}
