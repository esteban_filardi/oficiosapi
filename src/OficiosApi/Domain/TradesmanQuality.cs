namespace OficiosApi.Domain
{
    public class TradesmanQuality
    {
        public string TradesmanQualityId { get; set; }
        public string Label { get; set; }
        public int SortOrder { get; set; }
    }
}
