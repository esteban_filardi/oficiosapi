using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace OficiosApi.Domain {    
    public class State   
    {
        public int Id { get; set; }        
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }        
        public List<City> Cities { get; set; }
    }
}
