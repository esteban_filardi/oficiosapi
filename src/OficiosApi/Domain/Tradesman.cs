using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using Newtonsoft.Json;

namespace OficiosApi.Domain
{
    public class Tradesman
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        public City City { get; set; }

        [MinLength(1)]
        public List<TradesmanSkilledTrade> TradesmanSkilledTrades { get; set; }

        public ApplicationUser User { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public List<Review> Reviews { get; set; }

        public float? AverageScore {get; set; }
                        
        public string ImageProfilePath {get; set;}

        public string ProfileMarkup { get; set; }

        public DateTime CreatedAt { get; set; }

        public Tradesman() 
        {
            Reviews = new List<Review>();
            TradesmanSkilledTrades = new List<TradesmanSkilledTrade>();
        }

        public void UpdateAverageScore()
        {
            if (null == Reviews || 0 == Reviews.Count) 
            {
                AverageScore = null;
                return;
            }

            var sum = Reviews
                .Where(review => review.AverageScore != null)
                .Sum(review => (float) review.AverageScore);

            AverageScore = sum / Reviews.Count;
        }
    }
}
