using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models.Input.SkilledTrade;

namespace OficiosApi.Controllers 
{
    [Route("api/[controller]")]
    [ApiController]
    public class SkilledTradesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public SkilledTradesController(ApplicationDbContext context) 
        {
            _context = context;
        }

        [HttpGet]
        public IEnumerable<SkilledTrade> Get() {
            return _context.SkilledTrades
                .OrderBy(s => s.Name);
        }


        [HttpPost]
        public ActionResult<SkilledTrade> Post(SkilledTradeDto skilledTradeDto)
        {
            var skilledTrade = new SkilledTrade {Name = skilledTradeDto.Name};

            _context.SkilledTrades.Add(skilledTrade);

            _context.SaveChanges();

            return Ok(skilledTrade);
        }
        
        [HttpGet("{skilledTradeId}")]
        public ActionResult<SkilledTrade> Get(int skilledTradeId)
        {
            var skilledTrade = _context.SkilledTrades.Find(skilledTradeId);
            return Ok(skilledTrade);
        }

        [HttpPut("{skilledTradeId}")]
        public ActionResult<SkilledTrade> Put(int skilledTradeId, SkilledTradeDto skilledTradeDto)
        {
            var skilledTrade = _context.SkilledTrades.Find(skilledTradeId);
            skilledTrade.Name = skilledTradeDto.Name;

            _context.SaveChanges();
            
            return Ok(skilledTrade);
        }
        
        [HttpDelete("{skilledTradeId}")]
        public ActionResult<SkilledTrade> Delete(int skilledTradeId)
        {
            var skilledTrade = _context.SkilledTrades.Find(skilledTradeId);
            _context.SkilledTrades.Remove(skilledTrade);

            try
            {
                _context.SaveChanges();
                return NoContent();
            }
            catch (DbUpdateException)
            {
                return Conflict();
            }
        }
    }
}