using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;

namespace OficiosApi.Controllers 
{
    [Route("api/[controller]")]
    [ApiController]
    public class StatesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public StatesController(ApplicationDbContext context)
        {
            _context = context;
        }
        

        [HttpGet]
        public IEnumerable<State> Get()
        {
            return _context.States
                .OrderBy(t => t.Name)
                .Include(t => t.Cities);
        }
    }
}
