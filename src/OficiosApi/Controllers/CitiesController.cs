using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;

namespace OficiosApi.Controllers 
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitiesController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CitiesController(ApplicationDbContext context)
        {
            _context = context;
        }
        

        [HttpGet]
        public IEnumerable<City> Get()
        {
            return _context.Cities
                .Include(t => t.State)
                .OrderBy(t => t.Name);
        }
    }
}
