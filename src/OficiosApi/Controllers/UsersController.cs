using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MimeKit;
using OficiosApi.Application.Services;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models;
using OficiosApi.Models.Input.Tradesman;

namespace OficiosApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public UsersController(ApplicationDbContext dbContext,
            IRazorViewToStringRenderer razorViewToStringRenderer,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration)
        {
            _dbContext = dbContext;
            _razorViewToStringRenderer = razorViewToStringRenderer;
            _userManager = userManager;
            _configuration = configuration;
        }
        
        [HttpPost("verificationEmails")]
        public async Task<IActionResult> VerificationEmails(VerificationEmailRequest verificationEmailRequest)
        {
            var userType = verificationEmailRequest.UserType;
            var email = verificationEmailRequest.Email;

            var emailWithPrefix = GetUserEmailWithPrefix(userType, email);

            var user = _dbContext.Users
                .FirstOrDefault(u => u.UserName == emailWithPrefix);

            if (user == null)
            {
                return NotFound();
            }
            
            var confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(user);

            GetFirstNameAndLastNameOfUser(user, userType, out var firstName, out var lastName);
            var message = new MimeMessage ();
            message.From.Add (new MailboxAddress ("TodoOficios", "esteban.filardi@gmail.com"));
            message.To.Add (new MailboxAddress ($"{firstName}  {lastName}", 
                email));
            message.Subject = "TodoOficios - Confirme su correo electrónico";

            var confirmAccountEmail = new ConfirmAccountViewModel
            {
                UserId = user.Id,
                FirstName = firstName,
                LastName = lastName,
                ConfirmationToken = confirmationToken,
                UserType = userType,
                ClientUrl = _configuration.GetValue<string>("ClientUrl")
            };

            var bodyBuilder = new BodyBuilder
            {
                HtmlBody = await _razorViewToStringRenderer.RenderViewToStringAsync(
                    "/Views/ConfirmAccountEmail.cshtml", confirmAccountEmail)
            };

            message.Body = bodyBuilder.ToMessageBody();

            using (var client = new SmtpClient()) 
            {
                client.Connect ("in-v3.mailjet.com", 587, false);
                var username = _configuration.GetValue<string>("Email:Smtp:Username");
                var password = _configuration.GetValue<string>("Email:Smtp:Password");
                client.Authenticate(username, password);
                client.Send(message);
                client.Disconnect(true);
            }

            return Ok();
        }
        
        [HttpPost("{id}/verify")]
        public async Task<ActionResult> PostVerifyUser(string id, [FromBody] ConfirmAccountDto confirmAccountDto)
        {
            var user = _dbContext.Users
                .Find(id);
            
            if (user == null)
            {
                return NotFound();
            }

            var confirmationToken = confirmAccountDto.ConfirmationToken;
            
            var result = await _userManager.ConfirmEmailAsync(user, confirmationToken);

            if (result.Succeeded)
            {
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        private void GetFirstNameAndLastNameOfUser(ApplicationUser user,
            string userType,
            out string firstName,
            out string lastName)
        {
            if (userType == "tradesman")
            {
                var tradesman = _dbContext.Tradesmen.FirstOrDefault(t => t.User.Id == user.Id);
                firstName = tradesman?.Name;
                lastName = tradesman?.LastName;
            }
            else
            {
                var client = _dbContext.Clients.FirstOrDefault(t => t.User.Id == user.Id);
                firstName = client?.FirstName;
                lastName = client?.LastName;
            }
        }

        private static string GetUserEmailWithPrefix(string userType, string email)
        {
            return userType == "tradesman" ? $"Tradesman-{email}" : $"Client-{email}";
        }
    }

    public class VerificationEmailRequest
    {
        public string UserType { get; set; }
        public string Email { get; set; }
    }
}