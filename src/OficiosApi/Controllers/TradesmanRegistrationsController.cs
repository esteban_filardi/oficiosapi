using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using MimeKit;
using OficiosApi.Application.Services;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models;
using OficiosApi.Models.Input.Tradesman;

namespace OficiosApi.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class TradesmanRegistrationsController : ControllerBase
    {
        public const string RoleName = "Tradesman";
        private readonly ApplicationDbContext _context;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public TradesmanRegistrationsController(
            ApplicationDbContext context,
            IRazorViewToStringRenderer razorViewToStringRenderer,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration)
        {
            _context = context;
            _razorViewToStringRenderer = razorViewToStringRenderer;
            _userManager = userManager;
            _configuration = configuration;
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]TradesmanRegistrationDto tradesmanRegistrationDto)
        {
            try
            {
                if (tradesmanRegistrationDto == null)
                {
                    return BadRequest();
                }

                if (!ModelState.IsValid)
                {
                    // ToDo: Doing it like this, the response includes the errors but the names of the keys
                    // are not camelcased. Find a way to do it.
                    return BadRequest(ModelState);
                }

                var tradesman = await CreateTradesman(tradesmanRegistrationDto);
                _context.Tradesmen.Add(tradesman);

                var confirmationToken = await _userManager.GenerateEmailConfirmationTokenAsync(tradesman.User);
            
                _context.SaveChanges();

                try
                {
                    await SendRegistrationEmail(tradesman, confirmationToken);
                }
                catch (Exception)
                {
                    // if the email sending fails, return a successful response anyway
                }

                return CreatedAtRoute("GetTradesman", new { clientId = tradesman.Id }, tradesman);
            }
            catch (EmailAlreadyBeingUsedException)
            {
                return Conflict();
            }
        }

        private async Task<Tradesman> CreateTradesman(TradesmanRegistrationDto tradesmanRegistrationDto)
        {
            var now = DateTime.Now;

            var user = await CreateUser(tradesmanRegistrationDto, now);

            var tradesman = new Tradesman
            {
                Name = tradesmanRegistrationDto.FirstName,
                LastName = tradesmanRegistrationDto.LastName,
                PhoneNumber = tradesmanRegistrationDto.PhoneNumber,
                CreatedAt = now
            };

            var city = _context.Cities
                .Include(c => c.State)
                .SingleOrDefault(c => c.Id == tradesmanRegistrationDto.CityId);

            tradesman.City = city;

            foreach (var skilledTradeId in tradesmanRegistrationDto.SkilledTradeIds)
            {
                var skilledTrade = _context.SkilledTrades
                    .SingleOrDefault(s => s.Id == skilledTradeId);


                var tradesmanSkilledTrade = new TradesmanSkilledTrade {SkilledTrade = skilledTrade};

                tradesman.TradesmanSkilledTrades.Add(tradesmanSkilledTrade);
            }

            tradesman.User = user;
                       
            return tradesman;
        }

        private async Task<ApplicationUser> CreateUser(TradesmanRegistrationDto tradesmanRegistrationDto, DateTime now)
        {
            var emailWithRolePrefix = $"{RoleName}-{tradesmanRegistrationDto.Email}";
            
            // check if the Username/Email already exists
            var user = await _userManager.FindByNameAsync(emailWithRolePrefix);
            if (user != null)
            {
                throw new EmailAlreadyBeingUsedException();
            }
            
            // added in 2018.01.06 to fix GitHub issue #11
            // ref.: https://github.com/PacktPublishing/ASP.NET-Core-2-and-Angular-5/issues/11
            // if (!PasswordCheck.IsValidPassword(model.Password, _userManager.Options.Password)) return BadRequest("Password is too weak.");

            // create a new Item with the client-sent json data
            user = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                UserName = emailWithRolePrefix,
                Email = tradesmanRegistrationDto.Email,
                DisplayName = tradesmanRegistrationDto.FirstName,
                CreatedDate = now,
                LastModifiedDate = now
            };

            // Add the user to the Db with the chosen password
            await _userManager.CreateAsync(user, tradesmanRegistrationDto.Password);

            // Assign the user to the 'RegisteredUser' role.
            await _userManager.AddToRoleAsync(user, RoleName);

            // Remove Lockout and E-Mail confirmation
            user.EmailConfirmed = false;
            user.LockoutEnabled = true;
            
            return user;
        }

        private async Task SendRegistrationEmail(Tradesman tradesman, string confirmationToken)
        {
            var message = new MimeMessage ();
            message.From.Add (new MailboxAddress ("TodoOficios", "esteban.filardi@gmail.com"));
            message.To.Add (new MailboxAddress ($"{tradesman.Name}  {tradesman.LastName}", 
                tradesman.User.Email.Replace($"{RoleName}-", "")));
            message.Subject = "TodoOficios - Confirme su correo electrónico";

            var confirmAccountEmail = new ConfirmAccountViewModel
            {
                UserId = tradesman.User.Id,
                UserType = "tradesman",
                FirstName = tradesman.Name,
                LastName = tradesman.LastName,
                ConfirmationToken = confirmationToken,
                ClientUrl = _configuration.GetValue<string>("ClientUrl")
            };

            var bodyBuilder = new BodyBuilder
            {
                HtmlBody = await _razorViewToStringRenderer.RenderViewToStringAsync(
                    "/Views/ConfirmAccountEmail.cshtml", confirmAccountEmail)
            };

            message.Body = bodyBuilder.ToMessageBody();

            using (var client = new SmtpClient ()) 
            {
                client.Connect ("in-v3.mailjet.com", 587, false);
                var username = _configuration.GetValue<string>("Email:Smtp:Username");
                var password = _configuration.GetValue<string>("Email:Smtp:Password");
                client.Authenticate(username, password);
                client.Send(message);
                client.Disconnect(true);
            }
        }
    }
    
    public class EmailAlreadyBeingUsedException : Exception
    {
    }
}
