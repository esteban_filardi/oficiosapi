using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models;
using OficiosApi.Models.Input;

namespace OficiosApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class AdminTokensController : ControllerBase
    {
        private RoleManager<IdentityRole> _roleManager;
        private UserManager<ApplicationUser> _userManager;

        public AdminTokensController(
            ApplicationDbContext context,
            RoleManager<IdentityRole> roleManager,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration
        )
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }
        
        [HttpGet("create")]
        public async Task<IActionResult> CreateAdminAction()
        {
            //initializing custom roles 
            string[] roleNames = { "Admin" };
            IdentityResult roleResult;

            foreach (var roleName in roleNames)
            {
                var roleExist = await _roleManager.RoleExistsAsync(roleName);
                // ensure that the role does not exist
                if (!roleExist)
                {
                    //create the roles and seed them to the database: 
                    roleResult = await _roleManager.CreateAsync(new IdentityRole(roleName));
                }
            }

            // find the user with the admin email 
            var user = await _userManager.FindByEmailAsync("admin@email.com");

            // check if the user exists
            if (user != null) return Ok();
            
            //Here you could create the super admin who will maintain the web app
            user = new ApplicationUser
            {
                UserName = "admin",
                Email = "admin@email.com",
            };
                
            const string adminPassword = "123456";

            var createPowerUser = await _userManager.CreateAsync(user, adminPassword);
            if (createPowerUser.Succeeded)
            {
                //here we tie the new user to the role
                await _userManager.AddToRoleAsync(user, "Admin");
            }

            return Ok();
        }
    }
}
