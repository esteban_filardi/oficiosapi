using System.Linq;
using Microsoft.AspNetCore.Mvc;
using OficiosApi.Infrastructure;

namespace OficiosApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TradesmanQualities : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;

        public TradesmanQualities(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet]
        public IActionResult GetTradesmanQualitiesAction()
        {
            return Ok(_dbContext.TradesmanQualities.OrderBy(tq => tq.SortOrder));
        }
    }
}