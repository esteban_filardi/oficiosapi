using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using OficiosApi.Application.Services;
using OficiosApi.Application.Services.Client;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models.Client;
using OficiosApi.Models.Input;

namespace OficiosApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClientsController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly IRazorViewToStringRenderer _razorViewToStringRenderer;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public ClientsController(
            ApplicationDbContext context,
            IRazorViewToStringRenderer razorViewToStringRenderer,
            UserManager<ApplicationUser> userManager,
            IConfiguration configuration)
        {
            _dbContext = context;
            _razorViewToStringRenderer = razorViewToStringRenderer;
            _userManager = userManager;
            _configuration = configuration;
        }

        public ActionResult<ClientViewModel[]> Get()
        {
            var clients = _dbContext.Clients
                .Include(c => c.User)
                .OrderBy(c => c.LastName)
                .ThenBy(c => c.FirstName);
            
            var clientViewModels = clients.Select(CreateClientViewModel);

            return Ok(clientViewModels);
        }
        
        [HttpGet("{id}")]
        public ActionResult<ClientViewModel> Get(int id)
        {
            var client = _dbContext.Clients
                .Include(c => c.User)
                .FirstOrDefault(c => c.Id == id);

            if (client == null)
            {
                return NotFound();
            }

            var clientViewModel = CreateClientViewModel(client);

            return Ok(clientViewModel);
        }

        [HttpPost]
        public async Task<ActionResult> RegisterModel(ClientRegistrationRequestDto clientRegistrationRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            var clientRegistrationService = new ClientRegistrationService(_dbContext,
                _razorViewToStringRenderer,
                _userManager,
                _configuration);

            try
            {
                await clientRegistrationService.RegisterClient(clientRegistrationRequest);
                return Ok();
            }
            catch (OficiosApi.Application.Services.Client.EmailAlreadyBeingUsedException)
            {
                return Conflict();
            }
        }

        [HttpPut("{clientId}/blocked")]
        public async Task<IActionResult> Lockout(int clientId)
        {
            var client = await _dbContext.Clients.Include(c => c.User)
                .FirstOrDefaultAsync(c => c.Id == clientId);

            if (client == null)
            {
                return NotFound();
            }

            client.User.Blocked = !client.User.Blocked;

            await _dbContext.SaveChangesAsync();
            
            return NoContent();
        }
        
        private static ClientViewModel CreateClientViewModel(Client client)
        {
            return new ClientViewModel
            {
                Id = client.Id,
                FirstName = client.FirstName,
                LastName = client.LastName,
                Email = client.User.Email.Replace($"{ClientRegistrationService.RoleName}-", ""),
                CreatedDate = client.CreatedDate,
                LastModifiedDate = client.LastModifiedDate,
                Blocked = client.User.Blocked
            };
        }
    }
}
