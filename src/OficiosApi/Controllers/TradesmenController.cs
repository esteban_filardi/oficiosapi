using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using OficiosApi.Application.Authentication;
using OficiosApi.Application.Services.Review;
using OficiosApi.Application.Services.Tradesman;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models;
using OficiosApi.Models.Input.Review;
using OficiosApi.Models.Input.Tradesman;
using OficiosApi.Models.Input.Tradesman.TradesmanUpdateProfile;
using OficiosApi.Models.Output.TradesmanDetail;
using OficiosApi.Models.Review;

namespace OficiosApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TradesmenController : ControllerBase
    {
        const int ResultsPerPage = 10;

        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _hostingEnvironment;
        private readonly UserManager<ApplicationUser> _userManager;

        public TradesmenController(ApplicationDbContext context,
            IWebHostEnvironment hostingEnvironment,
            UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;
        }

        [HttpGet]
        public IEnumerable<Tradesman> Get([FromQuery] TradesmenRequestDto tradesmenRequestDto)
        {
            var page = tradesmenRequestDto.page is int pageInt && pageInt > 0 ? pageInt : 1;

            var tradesmenQueryService = new TradesmenQueryService(_context);
            var queryResult = tradesmenQueryService.QueryTradesmen(tradesmenRequestDto, ResultsPerPage, page);
            
            Request.HttpContext.Response.Headers.Add("X-Total-Count", queryResult.TotalCount.ToString());

            return queryResult.PageResults;
        }

        [HttpGet("{clientId:int}", Name = "GetTradesman")]
        public TradesmanViewModel Get(int clientId, string userId)
        {
            var tradesman = _context.Tradesmen
                .Include(t => t.TradesmanSkilledTrades)
                .ThenInclude(ts => ts.SkilledTrade)
                .Include(t => t.City)
                .ThenInclude(c => c.State)
                .Include(t => t.Reviews)
                .ThenInclude(r => r.Client)
                .Include(t => t.User)
                .SingleOrDefault(t => t.Id == clientId || t.User.Id == userId);

            if (tradesman == null)
                return null;

            var tradesmanUser = tradesman.User;
            var tradesmanViewModel = new TradesmanViewModel
            {
                Id = tradesman.Id,
                Name = tradesman.Name,
                LastName = tradesman.LastName,
                PhoneNumber = tradesman.PhoneNumber,
                AverageScore = tradesman.AverageScore,
                Reviews = tradesman.Reviews,
                Email = tradesmanUser?.Email.Replace($"{TradesmanRegistrationsController.RoleName}-", ""),
                TradesmanSkilledTrades = tradesman.TradesmanSkilledTrades,
                DateCreated = tradesman.CreatedAt,
                ImageProfilePath = tradesman.ImageProfilePath,
                ProfileMarkup = ReplaceServerPlaceHolderForCurrentServerUrl(tradesman.ProfileMarkup),
                City = tradesman.City != null ? new CityViewModel
                {
                    Id = tradesman.City.Id,
                    Name = tradesman.City.Name,
                    State = new StateViewModel {Id = tradesman.City.State.Id, Name = tradesman.City.State.Name}
                } : null,
                Blocked = tradesmanUser?.Blocked ?? false
            };

            // Sorting is done in memory, because EF doesn't support ordering include collections
            // @ToDo: perform two queries or update when EF support this feature
            tradesmanViewModel.Reviews.Sort((r1, r2) => 
                r1.CreatedDate.Equals(r2.CreatedDate) ? 0 : (r1.CreatedDate < r2.CreatedDate) ? 1 : -1);

            return tradesmanViewModel;
        }
        
        // @todo: Refactor the search method to another endpoint an create a collection method to retrieve tradesmen
        [HttpGet("plural")]
        public TradesmanViewModel[] GetByUserId(string userId)
        {
            return new[]
            {
                Get(int.MaxValue, userId)
            };
        }

        [HttpGet("{tradesmanId}/reviews")]
        public IList<TradesmanReviewForUser> GetReviewsForUser(int tradesmanId)
        {
            var reviews = _context.Reviews
                .Include(r => r.Client)
                .Include(r => r.QualityScores)
                .ThenInclude(q => q.TradesmanQuality)
                .Where(r => r.Tradesman.Id == tradesmanId)
                .ToArray();
            
            var tradesmanReviewsForUser = new List<TradesmanReviewForUser>(reviews.Length);
            tradesmanReviewsForUser.AddRange(reviews.Select(review => new TradesmanReviewForUser
            {
                ReviewId = review.Id.ToString(),
                Anonymous = review.Anonymous,
                Client = !review.Anonymous ? new TradesmanReviewForUser.ReviewClient
                {
                    ClientId = review.Client.Id,
                    FirstName = review.Client.FirstName,
                    LastName = review.Client.LastName
                } : null,
                QualityScores = review.QualityScores,
                Text = review.Text,
                CreatedDate = review.CreatedDate,
                AverageScore = review.AverageScore
            }));

            return tradesmanReviewsForUser;
        }

        private string ReplaceServerPlaceHolderForCurrentServerUrl(string tradesmanProfileMarkup)
        {
            return tradesmanProfileMarkup?.Replace("{{ serverUrl }}", GetServerUrl());
        }

        private string GetServerUrl()
        {
            return $"{Request.Scheme}://{Request.Host}{Request.PathBase}";
        }
        
        [HttpPost]
        public IActionResult Post([FromBody] Tradesman tradesman)
        {
            if (tradesman == null)
            {
                return BadRequest();
            }

            _context.Tradesmen.Add(tradesman);
            _context.SaveChanges();

            Console.WriteLine("Tradesman saved to database");

            return CreatedAtRoute("GetTradesman", new {id = tradesman.Id}, tradesman);
        }

        [HttpPut("{tradesmanId}")]
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Roles = "Tradesman")]
        public async Task<IActionResult> Put(int tradesmanId, [FromBody] TradesmanUpdateProfileDto tradesmanUpdateProfileDto)
        {
            if (tradesmanUpdateProfileDto == null)
            {
                return BadRequest();
            }

            try
            {
                var tradesmanProfileUpdater = new TradesmanProfileUpdater(_context, _hostingEnvironment, Request);

                if (await _context.Tradesmen.FirstOrDefaultAsync(t => t.Id == tradesmanId &&
                                                                      t.User.Id == User.GetUserId()) == null)
                {
                    return Forbid(JwtBearerDefaults.AuthenticationScheme);
                }
                
                tradesmanProfileUpdater.UpdateProfile(tradesmanId, tradesmanUpdateProfileDto);
            }
            catch (TradesmanProfileUpdater.TradesmanNotFoundException)
            {
                return NotFound();
            }
            
            return Ok();
        }

        [HttpPost("{id}/reviews", Name = "PostReview")]
        public ActionResult<Review> PostReview(int id, [FromBody] ReviewViewModel reviewViewmodel)
        {
            try
            {
                var reviewCreator = new ReviewCreator(_context);
                reviewCreator.CreateReview(new ReviewCreator.ReviewCreatorParameters
                {
                    TradesmanId = id,
                    ClientId = reviewViewmodel.ClientId,
                    QualityScores = reviewViewmodel.QualityScores,
                    Text = reviewViewmodel.Text,
                    Anonymous = reviewViewmodel.Anonymous
                });
                
                return StatusCode(201);
            }
            catch
            {
                return BadRequest();
            }
        }
        
        [HttpPost("{id}/post-image")]
        public async Task<object> PostImage(IFormFile upload)
        {
            var webRootPath = _hostingEnvironment.WebRootPath;
            var newPath = Path.Combine(webRootPath, "api", "uploads");
            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
                        
            var extension = "." + upload.FileName.Split('.')[upload.FileName.Split('.').Length - 1];
            var fileName = Guid.NewGuid() + extension; // Create a new filename 
           
            var fullPath = Path.Combine(newPath, fileName);

            Console.WriteLine(fullPath);

            using (var stream = new FileStream(fullPath, FileMode.Create))
            {
                await upload.CopyToAsync(stream);
            }

            var baseUrl = GetServerUrl();
            
            return new
            {
                url = $"{baseUrl}/api/uploads/{fileName}"
            };
        }
        
        [HttpGet("{tradesmanId}/profile-markup")]
        public IActionResult ProfileMarkup(int tradesmanId)
        {
            var tradesman = _context.Tradesmen.Find(tradesmanId);

            return Ok(new
            {
                profileMarkup = tradesman.ProfileMarkup
            });
        }
        
        [HttpGet("for-admin")]
        public IEnumerable<TradesmanForAdmin> GetTradesmenForAdmin([FromQuery] TradesmenRequestDto tradesmenRequestDto)
        {
            var tradesmenQueryService = new TradesmenQueryService(_context);
            var queryResult = tradesmenQueryService.QueryTradesmenForAdmin();
            
            return queryResult;
        }
        
        [HttpPut("{tradesmanId}/blocked")]
        public async Task<IActionResult> Blocked(int tradesmanId)
        {
            var client = await _context.Tradesmen.Include(t => t.User)
                .FirstOrDefaultAsync(t => t.Id == tradesmanId);

            if (client == null)
            {
                return NotFound();
            }

            client.User.Blocked = !client.User.Blocked;

            await _context.SaveChangesAsync();
            
            return NoContent();
        }
    }
}

