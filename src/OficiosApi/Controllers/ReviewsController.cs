using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using OficiosApi.Application.Services.Review;
using OficiosApi.Infrastructure;
using OficiosApi.Models.Input.Review;
using OficiosApi.Models.Output.Review;

namespace OficiosApi.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ReviewsController : ControllerBase
    {
        readonly ApplicationDbContext _dbContext;
        
        public ReviewsController(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        [HttpGet("{reviewId}")]
        public IActionResult GetSingleReview(int? reviewId)
        {
            var review = _dbContext.Reviews
                .Include(r => r.Tradesman)
                .Include(r => r.Client)
                .Include(r => r.QualityScores)
                    .ThenInclude(q => q.TradesmanQuality)
                .SingleOrDefault(r => r.Id == reviewId);

            if (review == null)
            {
                return NotFound();
            }

            var reviewResponse = new Review
            {
                ReviewId = review.Id.ToString(),
                Tradesman = new Tradesman
                {
                    TradesmanId = review.Tradesman.Id.ToString(),
                    FirstName = review.Tradesman.Name,
                    LastName = review.Tradesman.LastName
                },
                Client = review.Client,
                QualityScores = review.QualityScores,
                Text = review.Text,
                Anonymous = review.Anonymous
            };
            
            return Ok(reviewResponse);
        }
        
        [HttpPut("{reviewId}")]
        public IActionResult Put(int reviewId, ReviewUpdateViewModel reviewUpdateData)
        {
            var reviewUpdater = new ReviewUpdater(_dbContext);
            
            try
            {
                reviewUpdater.UpdateReview(reviewId, reviewUpdateData);
            }
            catch (ReviewUpdater.ReviewNotFoundException)
            {
                return NotFound();
            }
            
            return NoContent();
        }

        public IActionResult Get(int? clientId, int? tradesmanId)
        {
            var reviewsQuery = _dbContext.Reviews.Where(r => r.Client.Id == clientId);

            if (tradesmanId != null)
            {
                reviewsQuery = reviewsQuery.Where(r => r.Tradesman.Id == tradesmanId);
            }
            
            var reviews = reviewsQuery
                .Include(r => r.Client)
                .Include(r => r.Tradesman)
                .ToArray();

            var reviewsForAdminRole = new List<ReviewForAdminRole>(reviews.Length);
            reviewsForAdminRole.AddRange(reviews.Select(review => new ReviewForAdminRole
            {
                Id = review.Id,
                ClientId = review.Client.Id,
                ClientName = $"{review.Client.FirstName} {review.Client.LastName}",
                TradesmanId = review.Tradesman.Id,
                TradesmanName = $"{review.Tradesman.Name} {review.Tradesman.LastName}",
                Score = review.AverageScore,
                Text = review.Text,
                CreatedDate = review.CreatedDate
            }));

            return Ok(reviewsForAdminRole);
        }
        
        [HttpDelete("{reviewId}")]
        public IActionResult Delete(int reviewId)
        {
            var reviewDeleter = new ReviewDeleter(_dbContext);
            reviewDeleter.DeleteReview(reviewId);
            
            return NoContent();
        }
    }
}
