using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http;
using System.Security.Authentication;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using OficiosApi.Application.Services.Client;
using OficiosApi.Data;
using OficiosApi.Domain;
using OficiosApi.Infrastructure;
using OficiosApi.Models.Input;

namespace OficiosApi.Controllers
{
    [Route("api")]
    [ApiController]
    public class TokensController : ControllerBase    
    {
        private UserManager<ApplicationUser> UserManager { get; }
        private ApplicationDbContext DbContext { get; }
        
        public TokensController(UserManager<ApplicationUser> userManager, ApplicationDbContext dbContext)
        {
            UserManager = userManager;
            DbContext = dbContext;
        }

        [HttpPost("tokens")]
        public async Task<IActionResult> Authentication(TokenRequest request)
        {
            try
            {
                var user = await GetUser(request);

                return Ok(new TokenResponse
                {
                    AccessToken = GenerateJwtToken(user.Id, request.Role)
                });
            }
            catch (InvalidCredentialException)
            {
                return Unauthorized();
            }
            catch (UserIsBlockException)
            {
                return StatusCode(StatusCodes.Status403Forbidden, new
                {
                    Type = "userIsBlocked"
                });
            }
            catch (UserNotEmailConfirmed)
            {
                return StatusCode(StatusCodes.Status403Forbidden, new 
                {
                    Type = "userIsNotEmailConfirmed" 
                });
            }
        }
        
        [HttpPost("facebook-tokens")]
        public async Task<IActionResult> FacebookAuthentication(ExternalLoginRequestViewModel requestModel)
        {
            try
            {
                const string facebookApiUrl = "https://graph.facebook.com/v4.0/";
                var queryString = $"me?access_token={requestModel.facebookAccessToken}&fields=id,first_name,last_name,email";
                
                string apiResponse = null;
                using (var c = new HttpClient())
                {
                    c.BaseAddress = new Uri(facebookApiUrl);
                    var response = await c
                        .GetAsync(queryString);

                    if (response.IsSuccessStatusCode)
                    {
                        apiResponse = await response.Content.ReadAsStringAsync();
                    }
                    else throw new Exception("Authentication error");
                };

                var facebookResponseDictionary = JsonConvert.DeserializeObject<Dictionary<string, string>>(apiResponse);
                var userLoginInfo = new UserLoginInfo("facebook", facebookResponseDictionary["id"], "Facebook");

                // Check if this user already registered himself with this external provider before
                var user = await UserManager.FindByLoginAsync(
                    userLoginInfo.LoginProvider, userLoginInfo.ProviderKey);
                
                if (user == null)
                {
                    // If we reach this point, it means that this user never tried to logged in
                    // using this external provider. However, it could have used other providers 
                    // and /or have a local account. 
                    // We can find out if that's the case by looking for his e-mail address.

                    var username = $"{ClientRegistrationService.RoleName}-{facebookResponseDictionary["email"]}";
                    user = await UserManager.FindByNameAsync(username);
                    if (user == null)
                    {
                        var now = DateTime.Now;
                        
                        user = await CreateUserFromFacebookData(facebookResponseDictionary, username, now);
                        var client = CreateClientFromFacebookData(facebookResponseDictionary, user, now);
                        await DbContext.Clients.AddAsync(client);
                        
                        await DbContext.SaveChangesAsync();
                    }

                    // Register this external provider to the user
                    var identityResult = await UserManager.AddLoginAsync(user, userLoginInfo);
                    if (identityResult.Succeeded)
                    {
                        // Persist everything into the Db
                        await DbContext.SaveChangesAsync();
                    }
                    else throw new Exception("Authentication error");
                }

                if (user.Blocked)
                {
                    return StatusCode(StatusCodes.Status403Forbidden);
                }
                
                // create & return the access token
                var accessToken = GenerateJwtToken(user.Id, "Client");
                return Ok(new TokenResponse
                {
                    AccessToken = accessToken
                });
            }
            catch (Exception ex)
            {
                // return a HTTP Status 400 (Bad Request) to the client
                return BadRequest(new {Error = ex.Message});
            }
        }
        
        private async Task<ApplicationUser> CreateUserFromFacebookData(
            IReadOnlyDictionary<string, string> facebookResponseDictionary,
            string username,
            DateTime now)
        {
            var user = new ApplicationUser()
            {
                SecurityStamp = Guid.NewGuid().ToString(),
                // ensure the user will have an unique username
                UserName = username,
                Email = facebookResponseDictionary["email"],
                DisplayName = facebookResponseDictionary["first_name"],
                CreatedDate = now,
                LastModifiedDate = now
            };

            // Add the user to the Db with a random password
            await UserManager.CreateAsync(user,
                DataHelper.GenerateRandomPassword());

            // Assign the user to the 'Client' role.
            await UserManager.AddToRoleAsync(user, "Client");

            // Remove Lockout and E-Mail confirmation
            user.EmailConfirmed = true;
            user.LockoutEnabled = false;
            return user;
        }
        
        private Client CreateClientFromFacebookData(
            IReadOnlyDictionary<string, string> facebookResponseDictionary,
            ApplicationUser user,
            DateTime now)
        {
            var client = new Client
            {
                FirstName = facebookResponseDictionary["first_name"],
                LastName = facebookResponseDictionary["last_name"],
                User = user,
                CreatedDate = now,
                LastModifiedDate = now
            };
            
            return client;
        }

        private static string GenerateJwtToken(string userId, string requestRole)
        {
            var now = DateTime.UtcNow;

            var issuerSigningKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes("OficiosRandomKey"));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = "OficiosAPI",
                Audience = "OficiosClients",
                Claims = new Dictionary<string, object>
                {
                    [JwtRegisteredClaimNames.Sub] = userId,
                    ["roles"] = new[] {requestRole}
                },
                Expires = now.Add(TimeSpan.FromDays(365)),
                SigningCredentials = new SigningCredentials(issuerSigningKey, SecurityAlgorithms.HmacSha256)
            };

            var securityTokenHandler = new JwtSecurityTokenHandler();
            return securityTokenHandler.WriteToken(securityTokenHandler.CreateJwtSecurityToken(tokenDescriptor));
        }

        private async Task<IdentityUser> GetUser(TokenRequest request)
        {
            var realUserName = $"{request.Role}-{request.Username}";
            
            var user = await UserManager.FindByNameAsync(realUserName);

            if (user == null || !await UserManager.CheckPasswordAsync(user, request.Password))
            {
                throw new InvalidCredentialException();
            }

            if (user.Blocked)
            {
                throw new UserIsBlockException();
            }
                
            if (!user.EmailConfirmed)
            {
                throw new UserNotEmailConfirmed();
            }

            return user;
        }
    }
    
    public class TokenRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Role { get; set; }
    }

    public class TokenResponse
    {
        public string AccessToken { get; set; }
    }

    public class UserIsBlockException : Exception
    {
    }
    
    public class UserNotEmailConfirmed : Exception
    {
    }
}