using System;

namespace OficiosApi.Models
{
    public class TradesmanForAdmin
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CityAndState { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Blocked { get; set; }
    }
}