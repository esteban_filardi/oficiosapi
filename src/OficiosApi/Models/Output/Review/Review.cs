using System.Collections.Generic;
using OficiosApi.Domain;

namespace OficiosApi.Models.Output.Review
{
    public class Review
    {
        public string ReviewId { get; set; }
        public Tradesman Tradesman { get; set; }
        public List<TradesmanQualityScore> QualityScores { get; set; }
        public string Text { get; set; }
        public Domain.Client Client { get; set; }
        public bool Anonymous { get; set; }
        
    }
    
    public class Tradesman
    {
        public string TradesmanId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}