using System;

namespace OficiosApi.Models.Output.Review
{
    public class ReviewForAdminRole
    {
        public int Id { get; set; }
        public float? Score { get; set; }
        public string Text { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public int TradesmanId { get; set; }
        public string TradesmanName { get; set; }
    }
}

