﻿using Newtonsoft.Json;

namespace OficiosApi.Models
{
    [JsonObject(MemberSerialization.OptOut)]
    public class TokenResponseViewModel
    {
        public string token { get; set; }
        public int expiration { get; set; }
        public string refresh_token { get; set; }
        public string userId { get; set; }
        public string tradesmanId { get; set; }
        public string clientId { get; set; }
        public string first_name;
        public string last_name { get; set; }
    }
}
