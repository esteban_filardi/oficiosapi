namespace OficiosApi.Models.Output.TradesmanDetail
{
    public class StateViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
