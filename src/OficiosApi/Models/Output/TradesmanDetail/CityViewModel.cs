using OficiosApi.Models.Output.TradesmanDetail;

namespace OficiosApi.Models.Output.TradesmanDetail
{
    public class CityViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public StateViewModel State { get; set; }
    }
}
