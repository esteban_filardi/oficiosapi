using System;
using System.Collections.Generic;
using OficiosApi.Domain;

namespace OficiosApi.Models.Output.TradesmanDetail
{
    public class TradesmanViewModel
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string LastName { get; set; }

        public CityViewModel City { get; set; }

        public List<TradesmanSkilledTrade> TradesmanSkilledTrades { get; set; }

        public string Email { get; set; }
        
        public string PhoneNumber { get; set; }
        
        public List<Domain.Review> Reviews { get; set; }

        public float? AverageScore {get; set; }
                        
        public string ImageProfilePath {get; set;}

        public string ProfileMarkup { get; set; }
        
        public bool Blocked { get; set; }
        
        public DateTime DateCreated { get; set; }
    }
}