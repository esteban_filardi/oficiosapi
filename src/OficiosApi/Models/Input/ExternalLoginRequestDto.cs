using Newtonsoft.Json;

namespace OficiosApi.Models.Input
{
    [JsonObject(MemberSerialization.OptOut)]
    public class ExternalLoginRequestViewModel
    {

        public string facebookAccessToken { get; set; }
    }
}
