using System.Collections.Generic;

namespace OficiosApi.Models.Input.Review
{
    public class ReviewUpdateViewModel
    {
        public List<TradesmanQualityScoreViewModel> QualityScores { get; set; }
        public string Text { get; set; }
        public bool Anonymous { get; set; }
    }
}
