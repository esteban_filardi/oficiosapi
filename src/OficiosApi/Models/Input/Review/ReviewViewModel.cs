using System.Collections.Generic;

namespace OficiosApi.Models.Input.Review
{
    public class ReviewViewModel
    {
        public int TradesmanId { get; set; }
        public int ClientId { get; set; }
        public List<TradesmanQualityScoreViewModel> QualityScores { get; set; }
        public string Text { get; set; }
        public bool Anonymous { get; set; }
    }
}