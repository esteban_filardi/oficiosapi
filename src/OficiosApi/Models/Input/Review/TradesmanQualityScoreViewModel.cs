namespace OficiosApi.Models.Input.Review
{
    public class TradesmanQualityScoreViewModel
    {
        public string TradesmanQualityId { get; set; }
        public float Score { get; set; }
    }
}