﻿using Newtonsoft.Json;

namespace OficiosApi.Models.Input
{
    [JsonObject(MemberSerialization.OptOut)]
    public class TokenRequestViewModel
    {
        public string grantType { get; set; }
        public string clientId { get; set; }
        public string clientSecret { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string refreshToken { get; set; }
    }
}
