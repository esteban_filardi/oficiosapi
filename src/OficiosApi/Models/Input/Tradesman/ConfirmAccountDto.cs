namespace OficiosApi.Models.Input.Tradesman
{
    public class ConfirmAccountDto
    {
        public string ConfirmationToken { get; set; }
    }
}
