namespace OficiosApi.Models.Input.Tradesman.TradesmanUpdateProfile
{
    public class Base64EncodedDtoProfileImage
    {
        public string Filename { get; set; }
        public string FileType { get; set; }
        public string Base64EncodedImage { get; set; }
    }
}