namespace OficiosApi.Models.Input.Tradesman
{
    public class TradesmenRequestDto
    {
        public int? page { get; set; }
        public int? skilledTradeId { get; set; }
        public int? cityId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}
