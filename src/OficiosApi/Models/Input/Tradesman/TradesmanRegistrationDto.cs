using System.ComponentModel.DataAnnotations;

namespace OficiosApi.Models.Input.Tradesman {
    public class TradesmanRegistrationDto {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public int? CityId { get; set; }
        [Required]
        public int[] SkilledTradeIds { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
