using System;
using System.Collections.Generic;
using OficiosApi.Domain;

namespace OficiosApi.Models.Review
{
    public class TradesmanReviewForUser
    {
        public string ReviewId { get; set; }
        public List<TradesmanQualityScore> QualityScores { get; set; }
        public string Text { get; set; }
        public ReviewClient Client { get; set; }
        public bool Anonymous { get; set; }
        public float? Score { get; set; }
        public DateTime CreatedDate { get; set; }
        public float? AverageScore { get; set; }

        public class ReviewClient
        {
            public int ClientId { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
        }
    }
}
