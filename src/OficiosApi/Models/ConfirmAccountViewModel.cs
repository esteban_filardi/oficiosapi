namespace OficiosApi.Models
{
    public class ConfirmAccountViewModel
    {
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ConfirmationToken { get; set; }
        public string ClientUrl { get; set; }
        public string UserType { get; set; }
    }
}
